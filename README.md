# Antes de rodar o servidor pela primeira vez

1. Clonar o projeto
2. Copiar **.env.example** e renomear para **.env**
3. Instalar as dependências rodando: `npm install`

# Rodar o servidor

1. Na pasta raíz rodar: `npm start`
